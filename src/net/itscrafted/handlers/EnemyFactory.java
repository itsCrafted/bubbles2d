/**
 * Handles the creation of PushBalls.
 */

package net.itscrafted.handlers;

import java.util.ArrayList;

import net.itscrafted.entity.Player;
import net.itscrafted.entity.PushBall;

public class EnemyFactory {
	
	private static ArrayList<PushBall> pushBalls;
	private static Player player;
	
	public static void init(ArrayList<PushBall> pb, Player p) {
		pushBalls = pb;
		player = p;
	}
	
	public static void spawn(double x, double y, double speed) {
		pushBalls.add(new PushBall(x, y, speed, player));
	}
	
	public static void properties(Property p) {
		
	}
	
}
