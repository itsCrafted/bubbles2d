package net.itscrafted.main;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Game {
	
	public static JFrame window;
	
	private static boolean lessVisible = true;
	private static boolean undecorated = true;
	
	public static int VISIBLE = 0;
	public static int INVISIBLE = 1;
	
	public static void main(String[] args) {
		
		window = new JFrame("Bubbles");
		window.add(new GamePanel());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);
		window.setUndecorated(undecorated);
		window.pack();
		window.setLocationRelativeTo(null);
		window.setVisible(true);
		window.setIconImage(new ImageIcon("res/winicon.png").getImage());
		
		if(lessVisible) window.setOpacity(0.88F);
		
	}
	
	public static void setCursor(int i) {
		Cursor c = null;
		if(i == INVISIBLE) {
			BufferedImage bi = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
			c = Toolkit.getDefaultToolkit().createCustomCursor(bi, new Point(0, 0), ".");
		}
		window.setCursor(c);
	}
	
}
